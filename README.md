# Hi, I'm Noelia Martínez Mamani. 👋
- I’m currently learning English.
- I'm looking to collaborate on projects with `React`.
- 💬 Ask me about game dev.

# You can find me

![Discord](https://img.shields.io/badge/Discord-7289DA?style=flat-square&logo=discord&logoColor=white)

<a href="https://twitter.com/nor_mmn" target="_blank">

![Twitter](https://img.shields.io/badge/Twitter-1DA1F2?style=flat-square&logo=twitter&logoColor=white) 
</a>

# 🛠 &nbsp;Tech Stack

![JavaScript](https://img.shields.io/badge/JavaScript-181717?style=flat-square&logo=javascript)
![HTML5](https://img.shields.io/badge/HTML5-E34F26?style=flat-square&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/CSS3-1572B6?style=flat-square&logo=css3&logoColor=white)
![PHP](https://img.shields.io/badge/PHP-777BB4?style=flat-square&logo=php&logoColor=white)
![Git](https://img.shields.io/badge/Git-f14e32?style=flat-square&logo=git&logoColor=f0efe7)
![MarkDown](https://img.shields.io/badge/Markdown-181717?style=flat-square&logo=markdown&logoColor=white)
![React](https://img.shields.io/badge/React-181717?style=flat-square&logo=react&logoColor=00d8ff)
![NextJs](https://img.shields.io/badge/Next-blue?style=flat-square&logo=next.js&logoColor=white)
![Angular](https://img.shields.io/badge/Angular-B52E31?style=flat-square&logo=angular&logoColor=white)
![Phaser](https://img.shields.io/badge/Phaser-darkviolet?style=flat-square&logo=phaser&logoColor=white)
![NodeJs](https://img.shields.io/badge/Node.js-215732?style=flat-square&logo=node.js&logoColor=white)
![Express](https://img.shields.io/badge/Express-white?style=flat-square&logo=express&logoColor=black)
![Laravel](https://img.shields.io/badge/Laravel-f55247?style=flat-square&logo=laravel&logoColor=white)
![Visual Studio Code](https://img.shields.io/badge/Visual%20Studio%20Code-f0efe7?style=flat-square&logo=visual-studio-code&logoColor=blue)

---

<a href="https://github.com/nor-mn" target="_blank">
  
![GitHub](https://img.shields.io/badge/GitHub-181717?style=flat-square&logo=github&logoColor=white)
</a>
